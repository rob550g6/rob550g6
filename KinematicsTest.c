//SIMPLE TEST CODE FOR KINEMATICS
#include "DeltaArmKinematics.h"
/////////////////////////////////////
//
// Simple main() to test delta arm code
//
int main()
{
  forward_kinematics_t * FK = (struct _forward_kinematics_t*) malloc(sizeof(struct _forward_kinematics_t));
  inverse_kinematics_t * IK = (struct _inverse_kinematics_t*) malloc(sizeof(struct _inverse_kinematics_t));
  // Define a simple case of pointing (almost) straight down
  double E[3]={0.2, 0, -2.0};
  int status, i;

  // Run inverse kinematics to solve for angles (theta)
  status = delta_calcInverse(E[0], E[1], E[2], IK);
  if (status==0) {
    printf("Error computing inverse kinematics - no solution exists.\n");
    return -1;
  }

  printf("Inverse kinematics solution:  theta = {%lf, %lf, %lf}\n",
	 IK->theta[0], IK->theta[1], IK->theta[2]);

  // Now run forward kinematics - should return original E values
  status = delta_calcForward(IK->theta[0], IK->theta[1], IK->theta[2], FK);
  if (status==0) {
    printf("Error computing forward kinematics - invalid joint vector.\n");
    return -1;
  }
  
  printf("Forward kinematics solution:  E = {%lf, %lf, %lf}\n",
	 FK->x0, FK->y0, FK->z0);

free(FK);
free(IK);

  return -1;
}
