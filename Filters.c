//Filtering Functions
#include <stdio.h>
#include <stdlib.h>

//Replaces data(i) with the average of data(i) and previous n values.  Reduces noise.
double Moving_Avg_Filter(double * data, int window, int index)
{
//When choosing the window (n), be careful not to introduce lag into the system.  Make it comparable to sampling rate (at 50Hz, oldest point in 5-pnt SMA will be .1 seconds old).
//Index is the current data value's position in the data matrix

double sum;
double average;
double dataf;
int j;

if (index<window)
{
	sum = 0;
	for(j=0;j<index;j++)
	  sum += data(index-j); //Takes last k values (index to zero) and sums them

	average = sum/index;
	dataf = average;	
}
else
{
	sum = 0;
	for(j=0;j<window;j++)
	  sum += data(index-j); //Takes last n values and sums them

	average = sum/window;
	dataf = average;
}

return dataf;
}

//Comparator function for Median Filter
int cmpfunc (const void * a, const void * b)
{
   return ( *(double*)a - *(double*)b );
}

//Replaces data(i) with the median of data(i) and previous n values.  Reduces outliers.
double Median_Filter(double * data1, int window, int index)
{
//When choosing the window (n), be careful not to introduce lag into the system.  Make it comparable to sampling rate (at 50Hz, oldest point in 5-pnt SMA will be .1 seconds old).
//Index is the current data value's position in the data matrix

int i,j;


if (index<window)
{
	for(i=0;i<index;i++)
		data[i] = data1[index-1];

	qsort(data, window-index, sizeof(double), cmpfunc);	
	if (window-index % 2) /* odd window size */ 
	{ 
	        j = index/2;
	        dataf = data[j];
	} else {
		   j = (index+1)/2;
		   dataf = (data[j]+data[j-1])/2;
	} 
} else {
	for(i=0;i<window;i++)
		data[i] = data1[index-1];

	qsort(data, window, sizeof(double), cmpfunc);
	if (window % 2) /* odd window size */ 
	{ 
	        j = index/2;
	        dataf = data[j];
	} else {
		   j = (index+1)/2;
		   dataf = (data[j]+data[j-1])/2;
	} 
}

return dataf;
}
