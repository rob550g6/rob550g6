# Add your targets (i.e. the name of your output program) here
BINARIES = pixy_driver pixy_example flightlab IMU_driver state_mode readLCM 

CC = gcc
CFLAGS = -g -Wall -std=gnu99 `pkg-config --cflags lcm gsl`
LDFLAGS = `pkg-config --libs lcm gsl` -lm
BINARIES := $(addprefix bin/,$(BINARIES))

.PHONY: all clean lcmtypes bbblib

all: lcmtypes bbblib $(BINARIES)

lcmtypes:
	@$(MAKE) -C lcmtypes

bbblib:
	@$(MAKE) -C bbblib

clean:
	@$(MAKE) -C lcmtypes clean
	@$(MAKE) -C bbblib clean
	rm -f *~ *.o #bin/*

%.o: %.c
	$(CC) $(CFLAGS) -c $^ -o $@

lcmtypes/%.o: lcmtypes/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

bin/pixy_driver: pixy_driver.o util.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/pixy_example: pixy_example.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

# Add build commands for your targets here
bin/flightlab: bbblib/bbb_pwm.o bbblib/bbb_init.o flightlab.o DeltaArmKinematics.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o lcmtypes/IMU_data_t.o lcmtypes/state_machine_t.o
	$(CC) -o $@ $^ $(LDFLAGS)

bin/IMU_driver: bbblib/bbb_i2c.o bbblib/bbb_init.o IMU_driver.o lcmtypes/IMU_data_t.o util.o
	$(CC)  -o $@ $^ $(LDFLAGS)

bin/state_mode: state_mode.o lcmtypes/state_machine_t.o util.o
	$(CC)  -o $@ $^ $(LDFLAGS)

bin/readLCM: readLCM.o lcmtypes/pixy_t.o lcmtypes/pixy_frame_t.o lcmtypes/IMU_data_t.o lcmtypes/state_machine_t.o
	$(CC) -o $@ $^ $(LDFLAGS)
