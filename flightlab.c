/*
	ROB550-GROUP 6-FLIGHT LAB
	VITTORIO BICHUCHER, KATELYN FRY, ZZUO
	MAIN FUNCTION
*/

#define EXTERN  // Needed for global data declarations in bbb.h

#include "bbblib/bbb.h"
#include "DeltaArmKinematics.h"

#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>

#include "lcmtypes/pixy_frame_t.h"
#include "lcmtypes/IMU_data_t.h"
#include "lcmtypes/state_machine_t.h"

#define NSERVO 3
#define DISTANCE 110.0 
#define DISTANCE_SQRT2 155.563491861 
#define CAMERA_DISTANCE 125.0
#define F 238.936
#define CYLINDER_HEIGHT 250.0

void print_matrix(gsl_matrix* matrix){
	int i, j;

	printf("Number of Rows in Matrix: %d\n",matrix->size1);
	printf("Number of Columns in Matrix: %d\n",matrix->size2);

	// iterates through the matrix to print out each value
	for (i=0;i<(matrix->size1);i++){
		for (j=0;j<(matrix->size2);j++){
			printf("%f\t", gsl_matrix_get(matrix,i,j));
		}
		printf("\n");
	}
	printf("\n");

}

gsl_matrix* multM(gsl_matrix *A, gsl_matrix *B){

	gsl_matrix *C;
	double res, sum;
	int dull;
	int i, ii;
	
	int rowA = A->size1;
	int colA = A->size2;
	int rowB = B->size1;
	int colB = B->size2;
	
	C = gsl_matrix_alloc(rowA, colB);
	
	if(colA != rowB){
		printf("Matrix dimensions must be valid");
		return 0;
	}
	
	for(i=0; i<rowA; ++i){
		for(ii=0; ii<colB; ++ii){ 
			dull = 0; res = 0; sum = 0;
			while(colA>dull){
				res = gsl_matrix_get(A,i,dull)*gsl_matrix_get(B,dull,ii);
				sum += res;
				dull += 1;
			}
			gsl_matrix_set(C,i,ii,sum);
		}
	}
	
	return C;
}

gsl_matrix * affine_calibration(double readings[], int16_t color[], int n){

	int i = 0;
	double init[2*n];
	int s, kk;

	gsl_matrix *A = gsl_matrix_calloc(2*n,6);
	gsl_matrix *b = gsl_matrix_calloc(2*n,1);
	gsl_matrix *At = gsl_matrix_calloc(6,2*n);
	gsl_matrix *ATA = gsl_matrix_alloc(6,6);
	gsl_matrix *ATA_inv = gsl_matrix_calloc(6,6);
	gsl_matrix *ps_inv = gsl_matrix_calloc(6, 2*n);
	gsl_matrix *x = gsl_matrix_calloc(6,1);
	gsl_matrix *R = gsl_matrix_calloc(3,3); 
	gsl_matrix *Rinv = gsl_matrix_calloc(3,3);
	gsl_matrix *xyz = gsl_matrix_calloc(3,1);
	gsl_matrix *size = gsl_matrix_calloc(3,1);
	
	gsl_permutation * perm = gsl_permutation_alloc(6);
	gsl_permutation * perm2 = gsl_permutation_alloc(3);

	for(i=0; i<n; ++i){		
		switch(color[i]){
		case(10):
			init[2*i] = DISTANCE;
			init[2*i+1] = 0;
			break;
		case(11):
			init[2*i] = 0;
			init[2*i+1] = -DISTANCE;
			break;
		case(19):
			init[2*i] = -DISTANCE;
			init[2*i+1] = 0;
			break;
		case(12):
			init[2*i] = 0;
			init[2*i+1] = DISTANCE;
			break;
		default:
			printf("\n\n\nCOLOR CODE DOES NOT EXIST, LEAVING PROGRAM\n\n\n");
			return xyz;
		}
	}


	gsl_matrix_set(size, 0, 0, 160);
	gsl_matrix_set(size, 1, 0, 100);
	gsl_matrix_set(size, 2, 0, 1);

	for(i=0; i<2*n; ++i)
		gsl_matrix_set(b, i, 0, init[i]);
	
	for(i=0; i<n; ++i){
		gsl_matrix_set(A, 2*i, 0, readings[2*i]);
		gsl_matrix_set(A, 2*i, 1, readings[2*i+1]);
		gsl_matrix_set(A, 2*i, 2, 1);

		gsl_matrix_set(A, 2*i+1, 3, readings[2*i]);
		gsl_matrix_set(A, 2*i+1, 4, readings[2*i+1]);
		gsl_matrix_set(A, 2*i+1, 5, 1.0);
	}
	
	gsl_matrix_transpose_memcpy(At, A);
	ATA = multM(At,A);
	
	gsl_linalg_LU_decomp(ATA, perm, &s);
	gsl_linalg_LU_invert(ATA, perm, ATA_inv);

	ps_inv = multM(ATA_inv, At);
	x = multM(ps_inv, b);
	
	for(i=0; i<3; ++i){
		gsl_matrix_set(R, 0, i, gsl_matrix_get(x, i, 0));
		gsl_matrix_set(R, 1, i, gsl_matrix_get(x, i+3, 0));
	}
	
	gsl_matrix_set(R, 2, 2, 1);

	gsl_linalg_LU_decomp(R, perm2, &kk);
	gsl_linalg_LU_invert(R, perm2, Rinv);

	gsl_matrix_set(xyz, 0, 0, gsl_matrix_get(Rinv, 0, 2));
	gsl_matrix_set(xyz, 1, 0, gsl_matrix_get(Rinv, 1, 2));
	gsl_matrix_set(xyz, 2, 0, 1);
 
	gsl_matrix_free(A);
	gsl_matrix_free(At);
	gsl_matrix_free(ps_inv);
	gsl_matrix_free(ATA);
	gsl_matrix_free(ATA_inv);
	gsl_matrix_free(b);
	gsl_matrix_free(x);
	gsl_permutation_free(perm);
	gsl_permutation_free(perm2);
	gsl_matrix_free(R);

	return xyz;
}

int double_tag(double x1, double y1, double ang1, double x2, double y2, double ang2, double *sol){

	double tol = 0.01;
	double x, y;
	
	x1 = x1 - 160.0; x2 = x2 - 160.0;
	y1 = -y1 + 100.0; y2 = -y2 + 100.0;

	ang1 = ang1*M_PI/180.0;
	ang2 = ang2*M_PI/180.0;

	if(abs(M_PI/2 - abs(ang1)) < tol){
		sol[0] = x1; sol[1] = y2;
		return 0;		
	} 
	
	if(abs(M_PI/2 - abs(ang2)) < tol){
		sol[0] = x2; sol[1] = y1;
		return 0;
	}

	x = ((y1-y2) - x1*tan(ang1) +x2*tan(ang2))/(tan(ang2)-tan(ang1));
	y = y1 + tan(ang1)*(x-x1);

	sol[0] = x; sol[1] = y;

	return 0;
}

int calc_pixy_pose(const pixy_frame_t *msg, state_t *state){
	
	gsl_matrix *goal = gsl_matrix_calloc(3,1);
	double px_dist = 0;
	double tag_dist[(msg->nobjects)*(msg->nobjects-1)/2];
	double min_tag_dist = 0;
	double two_tag_sol[2] = {0, 0};
	int status = -1;
	int dull = 0;
 
	if(msg->nobjects == 1){
		printf("\n\n NOT ENOUGH OBJECTS FOR ESTIMATION \n\n");
		return 1;

	} else if(msg->nobjects == 2){

		if(msg->objects[0].signature == msg->objects[1].signature){
			printf("\n\n COLORS ARE NOT UNIQUE \n\n");
			return 1;
		}

	for(int i = 0; i<2; ++i){		
		switch(msg->objects[i].signature){
			case(10):
			case(11):
			case(19):
			case(12):
				break;

			default: 
				printf("COLOR SIGNATURE DOES NOT EXIST\n");
				return 1;
		}
	}

		double_tag(msg->objects[0].x, msg->objects[0].y, msg->objects[0].angle, msg->objects[1].x, msg->objects[1].y, msg->objects[1].angle, two_tag_sol);
		min_tag_dist = sqrt(pow(msg->objects[0].x - msg->objects[1].x , 2) + pow(msg->objects[0].y - msg->objects[1].y,2));

		state->wayptsxyz[state->traj_cnt][0] = DISTANCE_SQRT2/min_tag_dist*two_tag_sol[0];
		state->wayptsxyz[state->traj_cnt][1] = DISTANCE_SQRT2/min_tag_dist*two_tag_sol[1]+CAMERA_DISTANCE; 
		state->wayptsxyz[state->traj_cnt][2] = -(DISTANCE_SQRT2*F/min_tag_dist-CYLINDER_HEIGHT-30);

		printf("%lf\n", min_tag_dist);
		printf("\n\n GOINT TO: \n\n");
		printf("%lf\n", state->wayptsxyz[state->traj_cnt][0]);
		printf("%lf\n", state->wayptsxyz[state->traj_cnt][1]);
		printf("%lf\n", state->wayptsxyz[state->traj_cnt][2]);

		if( (state->wayptsxyz[state->traj_cnt][0] > 130.0) || (state->wayptsxyz[state->traj_cnt][0] < -130.0) ){
			printf("\n\nCOLLIDING WITH THE LEG!\n\n");
			return 1;
		}

		if(state->wayptsxyz[state->traj_cnt][2]>-70){
      			printf("\n\n HEIGHT IS POSITIVE \n\n");
      			return 1;
		}

		status = delta_calcInverse(state->wayptsxyz[state->traj_cnt][0], state->wayptsxyz[state->traj_cnt][1], state->wayptsxyz[state->traj_cnt][2], state->waypts[state->traj_cnt]);

		if(status){
	     		printf("\n\n CAN'T REACH TARGET, TOO FAR!! \n\n");
	     		return 1;
		}
		
		printf("\n\n GOINT TO IN DEGREES: \n\n");
		printf("%lf\n", state->waypts[state->traj_cnt][0]);
		printf("%lf\n", state->waypts[state->traj_cnt][1]);
		printf("%lf\n", state->waypts[state->traj_cnt][2]);

		state->traj_cnt += 1; 

		printf("\n\n GOINT TO IN DEGREES: \n\n");
		printf("%lf\n", state->waypts[state->traj_cnt][0]);
		printf("%lf\n", state->waypts[state->traj_cnt][1]);
		printf("%lf\n", state->waypts[state->traj_cnt][2]);

		state->wayptsxyz[state->traj_cnt][0] = DISTANCE_SQRT2/min_tag_dist*two_tag_sol[0];
		state->wayptsxyz[state->traj_cnt][1] = DISTANCE_SQRT2/min_tag_dist*two_tag_sol[1]+CAMERA_DISTANCE; 
		state->wayptsxyz[state->traj_cnt][2] = -(DISTANCE_SQRT2*F/min_tag_dist-CYLINDER_HEIGHT);

		status = delta_calcInverse(state->wayptsxyz[state->traj_cnt][0], state->wayptsxyz[state->traj_cnt][1], state->wayptsxyz[state->traj_cnt][2], state->waypts[state->traj_cnt]);

		if(status){
	     		printf("\n\n CAN'T REACH TARGET, TOO FAR!! \n\n");
	     		return 1;
		}

		state->traj_cnt += 1; 

		return 0;

	} else if(msg->nobjects == 4 || msg->nobjects == 3) {
		
		state->time = msg->utime;

		double readings[2*msg->nobjects];
		int16_t color[msg->nobjects];

		for (int i = 0; i < msg->nobjects; ++i){
			color[i] = msg->objects[i].signature;
			printf("color[%d] = %d\n\n", i, color[i]);

			switch(msg->objects[i].signature){
				case(10):
					readings[2*i] = (double)msg->objects[i].x;
					readings[2*i+1] = (double)msg->objects[i].y;
					break;

				case(11):
					readings[2*i] = (double)msg->objects[i].x;
					readings[2*i+1] = (double)msg->objects[i].y;
					break;

				case(19):
					readings[2*i] = (double)msg->objects[i].x;
					readings[2*i+1] = (double)msg->objects[i].y;
					break;

				case(12):
					readings[2*i] = (double)msg->objects[i].x;
					readings[2*i+1] = (double)msg->objects[i].y;
					break;

				default: 
					printf("COLOR SIGNATURE DOES NOT EXIST\n");
					return 1;
			}
		}
		
		for(int i=0; i<msg->nobjects; ++i){
			for(int ii=i+1; ii<msg->nobjects; ++ii){
				if(color[i] == color[ii]){
					printf("\n\n SOME COLORS ARE THE SAME \n\n");
					return 1;
				}
			}
		}

		goal = affine_calibration(readings, color, msg->nobjects);		
		
		if(gsl_matrix_get(goal, 2, 0) == 0){
			printf("\n\nAFFINE CALIBRATION FAILED\n\n");
			return 1;
		}

		px_dist = sqrt(pow(readings[0] - gsl_matrix_get(goal, 0, 0),2) + pow(readings[1] - gsl_matrix_get(goal,1,0),2));

		for(int i=0; i<msg->nobjects; ++i){
			for(int ii=i+1; ii<msg->nobjects; ++ii){
				printf("Comparing colors: %d, %d\n", color[i], color[ii]);
				if(((color[i] == 10) && (color[ii] == 19)) || ((color[ii] == 10) && (color[i] == 19)))
					continue;
				else if(((color[i] == 12) && (color[ii] == 11)) || ((color[ii] == 12) && (color[i] == 11)))
					continue;
				else if(color[i] == color [ii]){
					printf("\n\n COLORS ARE THE SAME \n\n");
					continue;
				}else{
					tag_dist[dull] = sqrt(pow(readings[2*i] - readings[2*ii], 2) + pow(readings[2*i+1] - readings[2*ii+1],2));
					dull += 1;
				}
			}
		}

		for(int i = 0; i<dull; ++i)
			min_tag_dist += tag_dist[i];
		
		min_tag_dist = min_tag_dist/dull;

		state->wayptsxyz[state->traj_cnt][0] = DISTANCE/px_dist*(gsl_matrix_get(goal, 0, 0)-160);
		state->wayptsxyz[state->traj_cnt][1] = DISTANCE/px_dist*(-gsl_matrix_get(goal, 1, 0)+100)+CAMERA_DISTANCE; 
		state->wayptsxyz[state->traj_cnt][2] = -(DISTANCE_SQRT2*F/min_tag_dist-CYLINDER_HEIGHT-30);

		if( (state->wayptsxyz[state->traj_cnt][0] > 130.0) || (state->wayptsxyz[state->traj_cnt][0] < -130.0) ){
			printf("\n\nCOLLIDING WITH THE LEG!\n\n");
			return 1;
		}

		if(state->wayptsxyz[state->traj_cnt][2]>-70){
      			printf("\n\n HEIGHT IS POSITIVE \n\n");
      			return 1;
		}

		status = delta_calcInverse(state->wayptsxyz[state->traj_cnt][0], state->wayptsxyz[state->traj_cnt][1], state->wayptsxyz[state->traj_cnt][2], state->waypts[state->traj_cnt]);

		if(status){
	     		printf("\n\n CAN'T REACH TARGET, TOO FAR!! \n\n");
	     		return 1;
		}

		//printf("STATE->WAYPTS:\n");
		//printf("%lf\n", state->wayptsxyz[state->traj_cnt][0]);
		//printf("%lf\n", state->wayptsxyz[state->traj_cnt][1]);
		//printf("%lf\n", state->wayptsxyz[state->traj_cnt][2]);

		state->traj_cnt += 1; 

		state->wayptsxyz[state->traj_cnt][0] = DISTANCE_SQRT2/min_tag_dist*two_tag_sol[0];
		state->wayptsxyz[state->traj_cnt][1] = DISTANCE_SQRT2/min_tag_dist*two_tag_sol[1]+CAMERA_DISTANCE; 
		state->wayptsxyz[state->traj_cnt][2] = -(DISTANCE_SQRT2*F/min_tag_dist-CYLINDER_HEIGHT);

		status = delta_calcInverse(state->wayptsxyz[state->traj_cnt][0], state->wayptsxyz[state->traj_cnt][1], state->wayptsxyz[state->traj_cnt][2], state->waypts[state->traj_cnt]);

		if(status){
	     		printf("\n\n CAN'T REACH TARGET, TOO FAR!! \n\n");
	     		return 1;
		}

		state->traj_cnt += 1; 
		return 0;

	} else {
		printf("\n\n TOO MANY DETECTIONS! DETECTIONS: %d \n\n", msg->nobjects);
		return 1;
	}
}


void clamping(double theta_g[3]){

	int it;
	for(it=0; it<NSERVO; ++it){
		if(theta_g[it] < -65){
			printf("Clamping Servo %d!!\n", it+1);
			theta_g[it] = -65;
		}
	}
}


void publish_walk(int servo_n, double ang){
	
	double pwm=0;


	switch(servo_n){
	case(0):
	      	pwm = ang*(12.68-7.5)/90+7.5;
		break;
	case(1):
	      	pwm = ang*(12.7-2.68)/180+(12.7+2.68)/2;  
		break;
	case(2):
	      	pwm = ang*(12.4-2.45)/180+(12.4+2.45)/2;
		break;
	case(3):
	      	pwm = ang*(12-3)/180+(12+3)/2;
		break;
	default: 
		printf("\n\nSERVO %d DOES NOT EXIST\n\n", servo_n);
		return;
	}

	bbb_setDutyPWM(servo_n, pwm);  // Duty cycle percent (0-100)
	
	printf("Running PWM at %.1f percent...\n",
	       bbb_getDutyPWM(servo_n));
	
	usleep(1000);
}

void walk(double *q){

	int it = 0;

	for(it=0; it<NSERVO; ++it)
		publish_walk(it, q[it]);

   	return;
}

void pickBall(state_t *state){

	if(state->retrieval == 1){
		publish_walk(3, 90);
		state->retrieval = 0;
	} else {
		publish_walk(3, -90);
		state->retrieval = 1;
	}

	usleep(500000);
	walk(state->pose_retrieval);
}

int initialize(state_t *state){
	
	int status;
	int it = 0;

	bbb_initPWM(0);
	if (bbb_initPWM(0))
		printf("Error initializing BBB PWM pin 0. Are you running as root?\n");
	bbb_setPeriodPWM(0, 20000000);  // Period is in nanoseconds
	bbb_setRunStatePWM(0, pwm_run);
	bbb_setPolarityPWM(0, pwm_straight);

	bbb_initPWM(1);
	if (bbb_initPWM(1))
		printf("Error initializing BBB PWM pin 1. Are you running as root?\n");
	bbb_setPeriodPWM(1, 20000000);  // Period is in nanoseconds
	bbb_setRunStatePWM(1, pwm_run);
	bbb_setPolarityPWM(1, pwm_straight);

	bbb_initPWM(2);
	if (bbb_initPWM(2))
		printf("Error initializing BBB PWM pin 2. Are you running as root?\n");
	bbb_setPeriodPWM(2, 20000000);  // Period is in nanoseconds
	bbb_setRunStatePWM(2, pwm_run);
	bbb_setPolarityPWM(2, pwm_straight);

	bbb_initPWM(3);
	if (bbb_initPWM(3))
		printf("Error initializing BBB PWM pin 3. Are you running as root?\n");
	bbb_setPeriodPWM(3, 20000000);  // Period is in nanoseconds
	bbb_setRunStatePWM(3, pwm_run);
	bbb_setPolarityPWM(3, pwm_straight);

	state->posexyz[0] = 0;
	state->posexyz[1] = 0;
	state->posexyz[2] = -55;
	status = delta_calcInverse(state->posexyz[0], state->posexyz[1], state->posexyz[2], state->pose_theta);
	if(status){
	     printf("AHA! INITIALIZATION FAIL! TOO BAD... \n\n");
	     return -1;
	}
	
	walk(state->pose_theta);
	sleep(1);
	publish_walk(3, -90.0);
	state->retrieval = 1;
	sleep(1);

	for(it = 0; it<NSERVO; ++it){
		state->pose_retrieval[it] = -65.0;
	}

	state->traj_cnt = 0;
	state->time = 0;
	state->cnt = 0;
	
	state->what_to_do = 'f';

	return 0;
}

void delete(state_t *state){

	if(!state)
		return;

	free(state);
}

gsl_matrix * TrajPlan (double *q0, double *v0, double *qf, double *vf, double t0, double tf){

	int motor_idx;
	int s;			// dummy variable for inv
	gsl_matrix * A = gsl_matrix_alloc(4,4);
	gsl_matrix * A_inv = gsl_matrix_alloc(4,4);
	gsl_permutation * perm = gsl_permutation_alloc(4);
	
	gsl_matrix * q = gsl_matrix_alloc(4,1);
	gsl_matrix * a = gsl_matrix_alloc(4,NSERVO);
	gsl_matrix* a_tmp;

	// Set Matrix A
	gsl_matrix_set(A, 0, 0, 1.0);
	gsl_matrix_set(A, 0, 1, t0);
	gsl_matrix_set(A, 0, 2, t0*t0);
	gsl_matrix_set(A, 0, 3, t0*t0*t0);

	gsl_matrix_set(A, 1, 0, 0.0);
	gsl_matrix_set(A, 1, 1, 1);
	gsl_matrix_set(A, 1, 2, 2*t0);
	gsl_matrix_set(A, 1, 3, 3*t0*t0);

	gsl_matrix_set(A, 2, 0, 1.0);
	gsl_matrix_set(A, 2, 1, tf);
	gsl_matrix_set(A, 2, 2, tf*tf);
	gsl_matrix_set(A, 2, 3, tf*tf*tf);

	gsl_matrix_set(A, 3, 0, 0.0);
	gsl_matrix_set(A, 3, 1, 1);
	gsl_matrix_set(A, 3, 2, 2*tf);
	gsl_matrix_set(A, 3, 3, 3*tf*tf);
	

	// Inv A
	////print_matrix(A);
	gsl_linalg_LU_decomp (A, perm, &s);
	gsl_linalg_LU_invert (A, perm, A_inv);

	// Free A
	gsl_matrix_free(A);
	gsl_permutation_free(perm);

	for (motor_idx = 0; motor_idx < NSERVO; ++motor_idx){
		// Set [q0, v0, qf, vf]
		gsl_matrix_set(q,0,0,q0[motor_idx]);
		gsl_matrix_set(q,1,0,v0[motor_idx]);
		gsl_matrix_set(q,2,0,qf[motor_idx]);
		gsl_matrix_set(q,3,0,vf[motor_idx]);
         
		// Get a = [a0, a1, a2, a3]
		a_tmp = multM(A_inv, q);
		gsl_matrix_set(a, 0, motor_idx, gsl_matrix_get(a_tmp,0,0));
		gsl_matrix_set(a, 1, motor_idx, gsl_matrix_get(a_tmp,1,0));
		gsl_matrix_set(a, 2, motor_idx, gsl_matrix_get(a_tmp,2,0));
		gsl_matrix_set(a, 3, motor_idx, gsl_matrix_get(a_tmp,3,0));

		// Free memory
		gsl_matrix_free(a_tmp);
	}

	gsl_matrix_free(A_inv);
	gsl_matrix_free(q);

	return a;
	
}

double * get_thetas (gsl_matrix * a, double t){

	gsl_matrix * q_tmp;
	gsl_matrix * time = gsl_matrix_alloc(1,4);
	int i;
	double *q = (double*)malloc(sizeof(double)*NSERVO);
    
	// Set matrix time
	gsl_matrix_set(time, 0, 0, 1.0);
	gsl_matrix_set(time, 0, 1, t);
	gsl_matrix_set(time, 0, 2, t*t);
	gsl_matrix_set(time, 0, 3, t*t*t);

	// Multiply
	q_tmp = multM(time, a);
       
        for (i=0;i<NSERVO;i++)
                q[i] = gsl_matrix_get(q_tmp,0,i);         
        
	// Free memory
	gsl_matrix_free(time);
	gsl_matrix_free(q_tmp);

	return q;
}

void createWaypoints(state_t *state){

	int flag=1;
	int status=0;
	while(flag==1){
			
		printf("Enter x, y, z goal location\n");
		scanf("%lf %lf %lf", &state->wayptsxyz[state->traj_cnt][0], &state->wayptsxyz[state->traj_cnt][1], &state->wayptsxyz[state->traj_cnt][2]);
		printf("%lf %lf %lf", state->wayptsxyz[state->traj_cnt][0], state->wayptsxyz[state->traj_cnt][1], state->wayptsxyz[state->traj_cnt][2]);
		status = delta_calcInverse(state->wayptsxyz[state->traj_cnt][0], state->wayptsxyz[state->traj_cnt][1], state->wayptsxyz[state->traj_cnt][2], state->waypts[state->traj_cnt]);

		if(status){
		      printf("Solution does not exist!\n\n");
		      continue;
		}

		state->traj_cnt += 1;
		clamping(state->waypts[state->traj_cnt]);
		printf("Enter another waypoint?[1/0]\n");
		scanf("%d",&flag);
	}
}

void state_machine_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const state_machine_t *msg, void *userdata){

	state_t *state =  (state_t *)userdata;
	state->what_to_do = msg->what_to_do;

	if(state->what_to_do == 'a'){
		int i = 0;
		do{
			walk(state->pose_retrieval);
			state->what_to_do = 'f';
			i += 1;
		}while(i<5);
	}		
}

void IMU_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const IMU_data_t *msg, void *userdata)
{

	state_t *state =  (state_t *)userdata;

	state->IMU_gyrox[state->cnt] = msg->gyro[0];
	state->IMU_gyroy[state->cnt] = msg->gyro[1];
	state->IMU_gyroz[state->cnt] = msg->gyro[2];

	state->IMU_accelx[state->cnt] = msg->accel[0];
	state->IMU_accely[state->cnt] = msg->accel[1];
	state->IMU_accelz[state->cnt] = msg->accel[2];

}

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
    	printf("Received message on channel %s, timestamp %" PRId64 "\n",
    	       channel, msg->utime);
	state_t *state =  (state_t *)userdata;

	if(state->what_to_do == 'p'){

		pickBall(state);
		state->what_to_do = 'f';

		return;

	} else if(state->what_to_do == 'r' || state->what_to_do == 'd'){

		if(msg->utime - state->time < 1000000)
			return;	

		//ADD IMU CONSTRAINT;

		// VARIABLES FOR TRAJECTORY PLANNING //
		double *q;
		gsl_matrix *space;
		double v[4] = {0, 0, 0, 0};
		double delta_t = 1;
		int it, it1;
		double t;
		int tester;
		state->traj_cnt = 0;

		tester = calc_pixy_pose(msg, state);

		if(tester != 0){
			printf("\n\n CALCULATING PIXY POSE FAILED! \n\n");
			return;
		}
		
		FILE *fp;
		fp = fopen("servo_data.txt","w+");
		
		for(it1=0; it1<state->traj_cnt; ++it1){

		//PERFORMING TRAJECTORY PLANNING FROM CURRENT STATE TO GOAL STATE //
			space = TrajPlan(state->pose_theta, v, state->waypts[it1], v, 0, delta_t);
			for(t = 0; t<=delta_t; t+=1){
				q = get_thetas(space, t);
				walk(q);
				usleep(1000);
				fprintf(fp, "%lf %lf %lf\n", q[0], q[1], q[2]);
			}
		
			delta_t =0.25;
		//UPDATE THE STATE ONCE THE MANIPULATOR GETS TO FINAL POSITION//
			for(it = 0; it<NSERVO; ++it){
				state->posexyz[it] = state->wayptsxyz[it1][it];
				state->pose_theta[it] = state->waypts[it1][it];
			}
		}

		fclose(fp);
		usleep(100000);
	

		state->what_to_do = 'f';
		pickBall(state);
		
		return;

	} else return;
}

int main(){

	// Initialize BBB and one PWM channel
	if (bbb_init()) {
		printf("Error initializing BBB.\n");
		return -1;
	}

	// GLOBAL VARIABLES THAT WE WILL USE THROUGHOUT CODE //
	state_t * state = (struct _state_t*)malloc(sizeof(struct _state_t));
	initialize(state);

	lcm_t *lcm = lcm_create(NULL);
	pixy_frame_t_subscription_t *pixy_sub = pixy_frame_t_subscribe(lcm, "PIXY", pixy_handler, state);
	//pixy_frame_t_subscription_set_queue_capacity(pixy_sub, 1);

	IMU_data_t_subscription_t *IMU_sub = IMU_data_t_subscribe(lcm, "IMU", IMU_handler, state);
	//IMU_data_t_subscription_set_queue_capacity(IMU_sub, 1);

	state_machine_t_subscription_t *state_machine_sub = state_machine_t_subscribe(lcm, "WHAT_TO_DO", state_machine_handler, state);
	//state_machine_t_subscription_set_queue_capacity(state_machine_sub, 1);


	//createWaypoints(state);

	while(1){
		lcm_handle(lcm);
	}


	delete(state);

	return 0;
}
