#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_cblas.h>


void foo(double *test){

	test[0] = 10.0; 
	test[1] = 3.14;
	test[2] = 5.16;

	return;

}
int main(){

	double test[1][3];
	test[0][0] = 0;
	test[0][1] = 0;
	test[0][2] = 0;
	printf("Test[1][1] = %lf\n", test[0][0]);
	printf("Test[1][2] = %lf\n", test[0][1]);
	printf("Test[1][3] = %lf\n", test[0][2]);

	printf("Going into foo\n");
	foo(test[0]);
	printf("Left foo\n");
	printf("Test[1][1] = %lf\n", test[0][0]);
	printf("Test[1][2] = %lf\n", test[0][1]);
	printf("Test[1][3] = %lf\n", test[0][2]);
	
	return 1;
}
