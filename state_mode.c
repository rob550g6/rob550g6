#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <lcm/lcm.h>
#include "lcmtypes/state_machine_t.h"
#include "util.h"

int main(){

	char what_to_do;
 	const char *channel = "WHAT_TO_DO";
	
	lcm_t *lcm = lcm_create(NULL);

	state_machine_t msg ={};

	while(1){
	
		printf("\n\n WHAT DO YOU WANT TO DO??? \n");
		printf("prepare(p), retrieve(r), drop(d), fly(f), abort(a):\n");
		scanf("%c", &what_to_do);
		
		if(what_to_do == 'p' || what_to_do == 'r' || what_to_do == 'd' || what_to_do == 'f' || what_to_do == 'a'){
			msg.what_to_do = what_to_do;
			msg.utime = utime_now();
		
			state_machine_t_publish(lcm, channel, &msg);
			usleep(50000);
		} else continue;
	}

	return 0;
}

