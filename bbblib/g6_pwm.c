 /*
  *  Beaglebone Black Hardware Interface Function Library
  *  For Use in Robotics 550, University of Michigan
  *
  *  References:  
  *      http://beagleboard.org/
  *      BlackLib: http://free.com/projects/blacklib
  *
  *  bbb_test_PWM.c:  Tests PWM for one servo channel
  *
  */

#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbb.h"

//#define PWM_CHOICE PWM_SV2  // set up choice of channel for each test

int main()
{
  int i, perc;
  double percent;
  int PWM_CHOICE;
  printf("Enter PWM channel #.\n");
  scanf("%d", &PWM_CHOICE);

  // Initialize BBB and one PWM channel

  if (bbb_init()) {
    printf("Error initializing BBB.\n");
    return -1;
  }

  if (bbb_initPWM(0)) {
    printf("Error initializing BBB PWM channel 0");
    return -1;
  }

  // Setup PWM (ask user for duty cycle %)
  printf("Enter PWM duty cycle in percent (0-100):\n");
  scanf("%lf", &percent);

  // The PWM hardware is set up so that channels 0 and 1 share the same period
  // and run state, and channels 2 and 3 are similarly paired
  bbb_setPeriodPWM(0, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(0, percent);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(0, pwm_run);

	usleep(1000);

  if (bbb_initPWM(1)) {
    printf("Error initializing BBB PWM channel 2");
    return -1;
  }


  // The PWM hardware is set up so that channels 0 and 1 share the same period
  // and run state, and channels 2 and 3 are similarly paired
  bbb_setPeriodPWM(1, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(1, percent);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(1, pwm_run);


	usleep(1000);

  if (bbb_initPWM(2)) {
    printf("Error initializing BBB PWM channel 2");
    return -1;
  }


  // The PWM hardware is set up so that channels 0 and 1 share the same period
  // and run state, and channels 2 and 3 are similarly paired
  bbb_setPeriodPWM(2, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(2, percent);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(2, pwm_run);



	usleep(1000);

  if (bbb_initPWM(3)) {
    printf("Error initializing BBB PWM channel 2");
    return -1;
  }


  // The PWM hardware is set up so that channels 0 and 1 share the same period
  // and run state, and channels 2 and 3 are similarly paired
  bbb_setPeriodPWM(3, 20000000);  // Period is in nanoseconds
  bbb_setDutyPWM(3, percent);  // Duty cycle percent (0-100)
  bbb_setRunStatePWM(3, pwm_run);



  printf("Running PWM at %.1f percent for 10 seconds...\n",
	 bbb_getDutyPWM(PWM_CHOICE));
       


  sleep(1);  // Run for 10 seconds

  // Stop PWM & exit

//  bbb_setDutyPWM(PWM_CHOICE, 0);

  return 0;
}
