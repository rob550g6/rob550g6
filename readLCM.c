#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>

#include "lcmtypes/pixy_frame_t.h"
#include "lcmtypes/IMU_data_t.h"
#include "lcmtypes/state_machine_t.h"


void state_machine_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const state_machine_t *msg, void *userdata){

	FILE *state =  (FILE *)userdata;
	fprintf(state, "%c\n", msg->what_to_do);
	fflush(state);	
}

void IMU_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const IMU_data_t *msg, void *userdata)
{

	FILE *imu =  (FILE *)userdata;
	fprintf(imu, "%lf %lf %lf %lf %lf %lf\n", msg->gyro[0],msg->gyro[1],msg->gyro[2],msg->accel[0],msg->accel[1],msg->accel[2]);
	
	fflush(imu);	

}

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
	FILE *pixy =  (FILE *)userdata;
	for(int i = 0; i<msg->nobjects; ++i)
		fprintf(pixy, "%d %d %d %d %d %d\n", i, msg->objects[i].x, msg->objects[i].y, msg->objects[i].angle, msg->objects[i].width, msg->objects[i].height);
	
	fflush(pixy);	
}

int main(){


	FILE *imu, *state, *pixy;
	imu = fopen("imu_data.txt", "w+");
	state = fopen("state_data.txt", "w+");
	pixy = fopen("pixy_data.txt", "w+");

	lcm_t *lcm = lcm_create(NULL);
	pixy_frame_t_subscription_t *pixy_sub = pixy_frame_t_subscribe(lcm, "PIXY", pixy_handler, pixy);
	IMU_data_t_subscription_t *IMU_sub = IMU_data_t_subscribe(lcm, "IMU", IMU_handler, imu);
	state_machine_t_subscription_t *state_machine_sub = state_machine_t_subscribe(lcm, "WHAT_TO_DO", state_machine_handler, state);
	

	while(1){
		lcm_handle(lcm);
	}

	return 0;
}
