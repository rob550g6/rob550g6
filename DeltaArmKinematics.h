
#ifndef __DELTAARMKINEMATICS_H__
#define __DELTAARMKINEMATICS_H__

#include "structures.h"

int delta_calcForward(double theta1, double theta2, double theta3,  double *theta);
int delta_calcAngleYZ(double x0, double y0, double z0, double * theta);
int delta_calcInverse(double x0, double y0, double z0, double *xg);
#endif //__DELTAARMKINEMATICS_H__
