///This file holds all structures made in this flight lab
#ifndef __STRUCTURES_H__
#define __STRUCTURES_H__

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

//STRUCTURE DEFINITIONS
//=======================================================================================
typedef struct _state_t state_t;
struct _state_t{
	
	int64_t time;
	
	double posexyz[3];
	double pose_theta[3];	
	double pose_buffer[3];
	
	int retrieval; 
	double pose_retrieval[3];

	int traj_cnt;
	double waypts[100][3];
	double wayptsxyz[100][3];

	double IMU_accelx[100];
	double IMU_accely[100];
	double IMU_accelz[100];

	double IMU_gyrox[100];
	double IMU_gyroy[100];
	double IMU_gyroz[100];

	int cnt;
	
	char what_to_do;
	
};

#endif //__STRUCTURES_H__
